enum {
	Q_X,
	Q_Y,
	Q_Z,
	V_X,
	V_Y,
	V_Z,
	MASS
};
/*enum for vectors with coordinates and mass*/

#define PI 3.1415926535897932384626433832795l
void verlet(planet *bod,const int N,const float phG,const float h);
