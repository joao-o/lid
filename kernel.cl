__kernel void SAXPY (__global float* x, 
                     __global float* m, 
                     int N, float h, float phG)
{
   const int i = get_global_id (0);
   __private int j;
   __private float d;
   __private float xi[6];
   /*__private int f=-1;*/
   
   xi[0]=x[6*i  ];
   xi[1]=x[6*i+1];
   xi[2]=x[6*i+2];
   xi[3]=x[6*i+3];
   xi[4]=x[6*i+4];
   xi[5]=x[6*i+5];

       for(j=0;j<i;j++){   
        d=(sqrt(  ( 
             (xi[0]  -x[6*j  ] )*(xi[0]-x[6*j  ])
            +(xi[1]  -x[6*j+1] )*(xi[1]-x[6*j+1])
            +(xi[2]  -x[6*j+2] )*(xi[2]-x[6*j+2])
               )* (               
             (xi[0]  -x[6*j  ] )*(xi[0]-x[6*j  ])
            +(xi[1]  -x[6*j+1] )*(xi[1]-x[6*j+1])
            +(xi[2]  -x[6*j+2] )*(xi[2]-x[6*j+2])
               )* (
             (xi[0]  -x[6*j  ] )*(xi[0]-x[6*j  ])
            +(xi[1]  -x[6*j+1] )*(xi[1]-x[6*j+1])
            +(xi[2]  -x[6*j+2] )*(xi[2]-x[6*j+2])
               )) + 1e-35); 
        
           /* if(d<1e-6)
                f=j;*/

            xi[3] += h*phG*m[j]*(x[6*j+0]-xi[0])/d;
            xi[4] += h*phG*m[j]*(x[6*j+1]-xi[1])/d;
            xi[5] += h*phG*m[j]*(x[6*j+2]-xi[2])/d;
              
       }
    /* evitar j=i sem usar ifs dentro de fors*/
      for(j=i+1;j<N;j++){
            d=(sqrt(  ( 
             (xi[0]  -x[6*j  ] )*(xi[0]-x[6*j  ])
            +(xi[1]  -x[6*j+1] )*(xi[1]-x[6*j+1])
            +(xi[2]  -x[6*j+2] )*(xi[2]-x[6*j+2])
               )* (               
             (xi[0]  -x[6*j  ] )*(xi[0]-x[6*j  ])
            +(xi[1]  -x[6*j+1] )*(xi[1]-x[6*j+1])
            +(xi[2]  -x[6*j+2] )*(xi[2]-x[6*j+2])
               )* (
             (xi[0]  -x[6*j  ] )*(xi[0]-x[6*j  ])
            +(xi[1]  -x[6*j+1] )*(xi[1]-x[6*j+1])
            +(xi[2]  -x[6*j+2] )*(xi[2]-x[6*j+2])
               )) + 1e-35); 
            
            /*if(d<1e-6)
                f=j;*/

            xi[3] += h*phG*m[j]*(x[6*j+0]-xi[0])/d;
            xi[4] += h*phG*m[j]*(x[6*j+1]-xi[1])/d;
            xi[5] += h*phG*m[j]*(x[6*j+2]-xi[2])/d;
                }
    

    xi[0] += h*xi[3];
    xi[1] += h*xi[4];
    xi[2] += h*xi[5];
     
    /*if(f!=-1){
    x[6*i  ]=x[6*f  ];
    x[6*i+1]=x[6*f+1];
    x[6*i+2]=x[6*f+2];
    x[6*i+3]=x[6*f+3];
    x[6*i+4]=x[6*f+4];
    x[6*i+5]=x[6*f+5];
        }
    else{ */      
    x[6*i  ]=xi[0];
    x[6*i+1]=xi[1];
    x[6*i+2]=xi[2];
    x[6*i+3]=xi[3];
    x[6*i+4]=xi[4];
    x[6*i+5]=xi[5];
    
}
