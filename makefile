W_FLAGS    = -Wall -Wextra -std=c99 -pedantic

OFLAGS     = -Ofast -flto -march=native -floop-interchange -ftree-loop-distribution -floop-strip-mine -floop-block -ftree-vectorize 

CFLAGS     = $(W_FLAGS) -g `sdl2-config --cflags` `pkg-config --cflags freetype2` \
             $(OFLAGS)
LFLAGS     = $(W_FLAGS) $(OFLAGS) -fuse-linker-plugin
LIBS       = `sdl2-config --libs` `pkg-config --libs freetype2` \
             -lGL -lGLU -lepoxy -lm -lOpenCL

CC =  gcc
RM = /bin/rm

TARGET = periapsis
CFILES := $(wildcard *.c)
OBJECT := $(patsubst %.c, %.o, ${CFILES})
HEADER := $(wildcard *.h)

all: comp link 

link: $(TARGET)

comp: $(OBJECT) 

%.o: %.c $(HEADER)
	$(CC) $(CFLAGS) -c $<

$(TARGET): $(OBJECT)
	$(CC) $(LFLAGS) -o $@ $^ $(LIBS)

edit:
	vim Makefile

count:
	wc -l *.c

clean:
	$(RM) -f *~ *.o $(TARGET) 
