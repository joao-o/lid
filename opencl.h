typedef struct {
	cl_uint n_platforms;
	cl_platform_id *platforms;
	cl_uint n_devices;
	cl_device_id *devices;
	cl_context_properties context_properties[4];
	cl_context context;
	cl_kernel kernel;
	cl_program program;
	cl_command_queue queue;
} ocl_dat;


void ckoclerr(const char *msg,cl_int error);
void p_platinfo(cl_platform_id id, cl_uint i);
void p_devinfo(cl_device_id id, cl_uint i, cl_device_info param_name, char *msg);
cl_program read_program(const char *path, cl_context context);
void init_openCL(ocl_dat *ocl);
void release_opencl(ocl_dat *ocl);
