

void sdldie(const char *msg);
void die(const char *msg);
void *xmalloc(size_t size);
void *xcalloc(size_t nmembs, size_t size);
void *xrealloc(void *ptr, size_t size);
