void draw_menu(void);
int enter_entry(cam *cam);
void edit_loop(cam *cam);
void draw_edit_menu(const char *buf,unsigned int bpos);
void resize_window_text(cam *cam);
int menu_loop(cam *cam);
void init_menu(one *ring);
void start_menu(void);
