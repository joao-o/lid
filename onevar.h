typedef struct {
	float x,y,z;
} vec3;

typedef struct {
	vec3 *p;
	int igh;
	int ngh;
} ghost;

typedef struct {
	vec3 p,v;
	float m;
	float rad;
	ghost gh;
} planet;

typedef struct {
  GLUquadric *quad;
  float radsc;
  uint8_t balls;
  uint8_t ghf;
} drawd;

#define FZERO -2
#define FCOM -1
typedef struct {
	vec3 p;
	int i;
	uint8_t flg;
} focus;

typedef struct {
	int w,h;
	float z;
	focus f;
	struct {
		float r,t,p;
	} ang;
   SDL_GLContext *glct;
   SDL_Window *win;
} cam; 

typedef struct {
  float phG;
  float h;
  int nstep;
  uint8_t pause;
  int N;
  planet *bod;

  drawd dr;
  cam cam;

  ocl_dat ocl;
  cl_mem x_buf, m_buf;
  cl_int err;
  size_t global_work_size[1];
} one;

/* var to rule them all, one var to find them
 * one var to bring them all, and in the code bind them */

void mtdoom(one * ring);
void frodo(one *ring);
void startsystem(one * ring, float **ax,
		 int fN, float fphG, float fh, int fnghosts);
void turn(one * ring);
void vshift(one* ring, int dn);
void seth(one *ring, float nh);
void spinDisplay(one * ring);
void IsLight(one * ring, int k);
void SetRadius(one * ring, int k, float r);
void SetRadMass(one *ring, float rmin, float rmax, float mmin, float mmax);
void prepCL(one *ring);
void setWH(one* ring, int w, int h);
