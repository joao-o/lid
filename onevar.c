#include <stdio.h>
#include <stdlib.h>

#include <SDL.h>
#include <epoxy/gl.h>
#include <epoxy/glx.h>
#include <GL/glu.h>
#include <SDL_opengl.h>

#include <CL/cl.h>

#include "opencl.h"
#include "onevar.h"
#include "util.h"
#include "verlet.h"

void
turn(one * ring)
{
	if (ring->pause) {

		/* if (ring->N > 50) {
		   for (k = 0; k < ring->nsteps; k++)
		   clEnqueueNDRangeKernel(ring->ocl.queue,

		   1, NULL,
		   ring->global_work_size, NULL, 0, NULL, NULL);

		   clEnqueueReadBuffer(ring->ocl.queue, ring->x_buf,
		   CL_TRUE, 0,
		   sizeof(float) * ring->N * 6, ring->x, 0, NULL, NULL);

		   } else */

		for (int k = 0; k < ring->nstep; k++)
			verlet(ring->bod,ring->N,ring->phG,ring->h);
		
		/*

		if (ring->nghosts != 0)
			for (s = 0; s < ring->N; s++)
				for (l = 0; l < 3; l++)
					ring->ghost[s][ring->gc * 3 + l] = ring->x[s * 6 + l];

		ring->agc++;
		if (ring->gc < ring->nghosts - 1)
			ring->gc++;
		else
			ring->gc = 0;

		*/
	}

}

void
prepCL(one * ring)
{
	/*
	   ring->x_buf = clCreateBuffer(ring->ocl.context,
	   CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR,
	   sizeof(float) * 6 * ring->N, ring->x, &(ring->err));
	   ckoclerr("alloc buffer 1", ring->err);

	   ring->m_buf = clCreateBuffer(ring->ocl.context,
	   CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR,
	   sizeof(float) * ring->N, ring->m, &(ring->err));
	   ckoclerr("alloc buffer 2", ring->err);

	   clSetKernelArg(ring->ocl.kernel, 0, sizeof(cl_mem), &(ring->x_buf));
	   clSetKernelArg(ring->ocl.kernel, 1, sizeof(cl_mem), &(ring->m_buf));
	   clSetKernelArg(ring->ocl.kernel, 2, sizeof(int), &(ring->N));
	   clSetKernelArg(ring->ocl.kernel, 3, sizeof(float), &(ring->h));
	   clSetKernelArg(ring->ocl.kernel, 4, sizeof(float), &(ring->phG));

	   int size = ring->N;
	   ring->global_work_size[0] = size;
	 */
}
