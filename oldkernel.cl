__kernel void SAXPY (__global float* x, 
                     __global float* m, 
                     __global int* key, 
                     __global int* rvk,
                     __global float* d, 
                     int N, int Nd, float h, float phG)
{
   const int i = get_global_id (0);
   __private int j;

   
    if(i<3*N)
    {
        x[6*(i/3)+i%3] += h*x[6*(i/3)+i%3+3];
    }
    
    barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
    
    if(i<Nd)
        {
        d[i]=1/(sqrt(  ( 
             (x[6*key[2*i]  ]  -x[6*key[2*i+1]  ] )*(x[6*key[2*i]  ]-x[6*key[2*i+1]  ])
            +(x[6*key[2*i]+1]  -x[6*key[2*i+1]+1] )*(x[6*key[2*i]+1]-x[6*key[2*i+1]+1])
            +(x[6*key[2*i]+2]  -x[6*key[2*i+1]+2] )*(x[6*key[2*i]+2]-x[6*key[2*i+1]+2])
               ))* sqrt((               
             (x[6*key[2*i]  ]  -x[6*key[2*i+1]  ] )*(x[6*key[2*i]  ]-x[6*key[2*i+1]  ])
            +(x[6*key[2*i]+1]  -x[6*key[2*i+1]+1] )*(x[6*key[2*i]+1]-x[6*key[2*i+1]+1])
            +(x[6*key[2*i]+2]  -x[6*key[2*i+1]+2] )*(x[6*key[2*i]+2]-x[6*key[2*i+1]+2])
               ))* sqrt((
             (x[6*key[2*i]  ]  -x[6*key[2*i+1]  ] )*(x[6*key[2*i]  ]-x[6*key[2*i+1]  ])
            +(x[6*key[2*i]+1]  -x[6*key[2*i+1]+1] )*(x[6*key[2*i]+1]-x[6*key[2*i+1]+1])
            +(x[6*key[2*i]+2]  -x[6*key[2*i+1]+2] )*(x[6*key[2*i]+2]-x[6*key[2*i+1]+2])
               )) + 1e-35); 
        }
        
    barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
        
    /* notar que i indica 3*N1+xi, i/3 indica N1, i%3 indica xi e j indica N2*/
    if(i<3*N)
    {
       for(j=0;j<i/3;j++){
              x[6*(i/3)+i%3+3] += h*phG*m[j]*d[rvk[(i/3)*N+j]]*(x[6*j+i%3]-x[6*(i/3)+i%3]);
            }
    /* evitar j=i/3 sem usar ifs dentro de fors*/
       for(j=i/3+1;j<N;j++){
              x[6*(i/3)+i%3+3] += h*phG*m[j]*d[rvk[(i/3)*N+j]]*(x[6*j+i%3]-x[6*(i/3)+i%3]);
            }
    
    }
    
 /*       x[6*key[2*i  ]+3] += m[key[2*i+1]]*h*phG*d[i]*(x[6*key[2*i+1]  ]-x[6*key[2*i  ]  ]);
        x[6*key[2*i+1]+3] += m[key[2*i]  ]*h*phG*d[i]*(x[6*key[2*i  ]  ]-x[6*key[2*i+1]  ]);

        x[6*key[2*i  ]+4] += m[key[2*i+1]]*h*phG*d[i]*(x[6*key[2*i+1]+1]-x[6*key[2*i  ]+1]);
        x[6*key[2*i+1]+4] += m[key[2*i]  ]*h*phG*d[i]*(x[6*key[2*i  ]+1]-x[6*key[2*i+1]+1]); 
        
        x[6*key[2*i  ]+5] += m[key[2*i+1]]*h*phG*d[i]*(x[6*key[2*i+1]+2]-x[6*key[2*i  ]+2]); 
        x[6*key[2*i+1]+5] += m[key[2*i]  ]*h*phG*d[i]*(x[6*key[2*i  ]+2]-x[6*key[2*i+1]+2]);*/

}
