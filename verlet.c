#include <SDL.h>
#include <CL/cl.h>
#include <epoxy/gl.h>
#include <epoxy/glx.h>
#include <GL/glu.h>
#include <stdio.h>
#include <math.h>

#include "util.h"
#include "opencl.h"
#include "onevar.h"

#define PH_G 1


float
dif(vec3 *v, vec3 *w)
{
	float dx,dy,dz,d; 
 	dx = v->x - w->x; 
	dy = v->y - w->y;
	dz = v->z - w->z;
	d = dx*dx+dy*dy+dz*dz;
	return sqrt(d*d*d);
}

vec3 
gf(planet *bod, const int planet ,const float phG,const int N)
{
	float ad;
	vec3 a= {.x=0,.y=0,.z=0};
	for (int i = 0; i < N; i++) {
			ad = phG * bod[i].m  / (dif(&(bod[i].p), &(bod[planet].p)));
			a.x -= ad*(bod[planet].p.x-bod[i].p.x);
			a.y -= ad*(bod[planet].p.y-bod[i].p.y);
			a.z -= ad*(bod[planet].p.z-bod[i].p.z);
	}
	return a;
}

void
verlet(planet *bod,const int N,const float phG,const float h)
{	
	vec3 a;
	for (int i = 0; i < N; i++){
			a=gf(bod,i,phG,N);
			bod[i].v.x += a.x * h;
			bod[i].v.y += a.y * h;
			bod[i].v.z += a.z * h;
	}

	for (int i = 0; i < N; i++) {
			bod[i].p.x += bod[i].v.x * h;
			bod[i].p.y += bod[i].v.y * h;
			bod[i].p.z += bod[i].v.z * h;
	}

	return;
}


