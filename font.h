void init_font(char *path);
float draw_string(const char *s,GLfloat x,GLfloat y,GLuint size, 
						const GLfloat color[3]);

float scrprintf(GLfloat x,GLfloat y,GLuint size, 
					const GLfloat color[3],const char *fmt,...);
