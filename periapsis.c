#define _POSIX_C_SOURCE 199309L
#include <stdio.h>
#include <stdlib.h>

#include <SDL.h>
#include <epoxy/gl.h>
#include <epoxy/glx.h>
#include <GL/glu.h>
#include <SDL_opengl.h>
#include <CL/cl.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#include <time.h>

#include "opencl.h"				  /* opencl.h requires <CL/cl.h> */
#include "onevar.h"				  /* onevar.h requires opencl.h */
#include "verlet.h"				  /* verlet.h requires onevar.h */

#include "font.h"
#include "draw.h"
#include "util.h"
#include "periapsis.h"
#include "list.h"
#include "menu.h"

#define SCR_W  640
#define SCR_H  640

/* global vars */

extern GLuint tex;
unsigned long int calctime;

#define OVRFTSZ 20
void
draw_overlay(one *ring)
{
	const GLfloat white[]={1.,1.,1.};
	float y;
	glDisable(GL_LIGHTING);
	y=-0.1-OVRFTSZ*P2CY;
	scrprintf(0.2,y,OVRFTSZ,white,"calc time: %ld us",
				 calctime/(CLOCKS_PER_SEC/1000000));
	y-=OVRFTSZ*P2CY*1.2;
	scrprintf(0.2,y,OVRFTSZ,white,"focus %f %f %f",
		ring->cam.f.p.x,ring->cam.f.p.y,ring->cam.f.p.z);
	y-=OVRFTSZ*P2CY*1.2;
	scrprintf(0.2,y,OVRFTSZ,white,"planet %f %f %f",
		ring->bod[0].p.x,ring->bod[0].p.y,ring->bod[0].p.z);

	glFlush();
	glEnable(GL_LIGHTING);
}

void
draw(one *ring)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	resize_window_text(&(ring->cam));
	draw_overlay(ring);

	glLoadIdentity();
	resize_window(&(ring->cam));
	if ( ring->cam.f.flg) focuspl(ring->bod,&(ring->cam),ring->N);
	setcam(&(ring->cam));

	drawbod(ring->bod,&(ring->dr),ring->N);

	glFlush();
	return;
}

void
resize_window(cam *cam)
{
	glClearColor(0, 0, 0, 0);
	glViewport(0, 0, (GLsizei) cam->w, (GLsizei) cam->h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-5.0 / SCR_W * cam->w * cam->z, 5.0 / SCR_W * cam->w * cam->z,
				 -5.0 / SCR_H * cam->h * cam->z, 5.0 / SCR_H * cam->h * cam->z, 
				 6, 120000.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glShadeModel(GL_SMOOTH);

	return;
}

void
initGL(cam *cam)
{
	GLfloat amb[] = { .20, .20, .20, .20 };
	glClearColor(1.0, 1.0, 1.0, 0.0);

	resize_window(cam);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


	glLightfv(GL_LIGHT0, GL_AMBIENT, amb);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_DEPTH_TEST);
	return;
}

void
initSDL(cam *cam)
{
	if (SDL_Init(SDL_INIT_VIDEO))
		sdldie("SDL could not initialize!");

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);

	if (!(cam->win = SDL_CreateWindow("SDL",
											  SDL_WINDOWPOS_UNDEFINED,
											  SDL_WINDOWPOS_UNDEFINED, SCR_W, SCR_H,
											  SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL |
											  SDL_WINDOW_RESIZABLE)))
		sdldie("SDL Windows creation failed!");

	if (!(cam->glct = SDL_GL_CreateContext(cam->win)))
		sdldie("OpenGL context could not be created!");

	printf("%s\n", glGetString(GL_VERSION));
	printf("Running on openGL version: %d\n",epoxy_gl_version());

	if (SDL_GL_SetSwapInterval(1))
		printf("Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError());
}

void
init(one *ring)
{
	puts("INIT");
	ring->N = 2;
	ring->phG = 1;
	ring->h = .01;
	ring->bod=xcalloc(ring->N, sizeof(planet));
	
	ring->bod[0].p.x=2;
	ring->bod[0].v.y=0.25;
	ring->bod[0].m=1;
	ring->bod[1].p.x=-2;
	ring->bod[1].v.y=-0.25;
	ring->bod[1].m=1;

	ring->cam.ang.r = 10;
	ring->cam.ang.t = PI / 2;
	
	ring->pause=1;
	ring->cam.z=1;
	ring->dr.quad = gluNewQuadric();
	gluQuadricNormals(ring->dr.quad, GLU_SMOOTH);
	
	ring->cam.w=SCR_W;
	ring->cam.h=SCR_H;
	ring->cam.f.i = -2;
	ring->cam.f.flg = 0;
	ring->dr.radsc = 1;

	ring->nstep = 10;
	return;
}


//int
//main(int argc, char **argv)
//{
//	(void)argc;
//	(void)argv;
//	SDL_Event ev;
//	one ring;
//	
//	init(&ring);
//
//	initSDL(&(ring.cam));
//	puts("SDL - loaded");
//	initGL(&(ring.cam));
//	puts("OpenGL - loaded");
//
//	init_font("Inconsolata-Regular.ttf");
//
//	init_menu(&ring);
//
//	for (;;) {
//		while (SDL_PollEvent(&ev)) {
//			switch (ev.type) {
//			case SDL_KEYDOWN:
//				switch (ev.key.keysym.sym) {
//				case SDLK_ESCAPE:
//					goto _quit;
//					break;
//				case SDLK_DOWN:
//					ring.cam.ang.t -= PI / 600.0;
//					if (ring.cam.ang.t < 0) ring.cam.ang.t = 0;
//					break;
//				case SDLK_UP:
//					ring.cam.ang.t += PI / 600.0;
//					if (ring.cam.ang.t > PI) ring.cam.ang.t = PI;
//					break;
//				case SDLK_RIGHT:
//					ring.cam.ang.p += PI / 300.0;
//					break;
//				case SDLK_LEFT:
//					ring.cam.ang.p -= PI / 300.0;
//					break;
//				case SDLK_i:
//					ring.cam.ang.r *= 0.9;
//					break;
//				case SDLK_o:
//					ring.cam.ang.r *= 1.1;
//					break;
//				case SDLK_g:
//					 /* FLIP(ring->gcflg); */
//					break;
//				case SDLK_s:
//					FLIP(ring.pause);
//					break;
//				case SDLK_PLUS:
//					focswitch(&(ring.cam.f),ring.N,1);
//					break;
//				case SDLK_MINUS:
//					focswitch(&(ring.cam.f),ring.N,-1);
//					break;
//				case SDLK_f:
//					FLIP(ring.cam.f.flg);
//					break;
//				case SDLK_p:
//					FLIP(ring.dr.balls);
//					break;
//				case SDLK_COMMA:
//					ring.nstep-=1;
//					if (ring.nstep < 1) {
//						printf("minimum velocity reached!\n"
//								 "for slower pace, reduce time step.\n");
//						ring.nstep=1;
//					}
//					break;
//				case SDLK_PERIOD:
//					ring.nstep+=1;
//					break;
//				case SDLK_z:
//					ring.cam.z *= 1.1;
//					break;
//				case SDLK_x:
//					ring.cam.z *= 0.9;
//					break;
//				case SDLK_y:
//					ring.dr.radsc *= 1.1;
//					break;
//				case SDLK_u:
//					ring.dr.radsc *= 0.9;
//					break;
//				case SDLK_m:
//					puts("open menu");
//					menu_loop(&(ring.cam));
//					break;
//				}
//
//			case SDL_WINDOWEVENT:
//				switch (ev.window.event) {
//				case SDL_WINDOWEVENT_SIZE_CHANGED:
//					ring.cam.w=ev.window.data1;
//					ring.cam.h=ev.window.data2;
//					break;
//				}
//			}
//
//		}
//		calctime=clock();
//		turn(&ring);
//		calctime=clock()-calctime;
//
//		draw(&ring);
//		SDL_GL_SwapWindow(ring.cam.win);
//	}
// _quit:
//
//	SDL_GL_DeleteContext(ring.cam.glct);
//	SDL_DestroyWindow(ring.cam.win);
//	SDL_Quit();
//	return EXIT_SUCCESS;
//}



#define M 3000
int main() 
{	
	planet bod[M];
	struct timespec cnow,cthen,rnow,rthen;

	for (int i=0;i<M;i++){
		bod[i].p.x=(rand()*100)/RAND_MAX;
		bod[i].p.y=(rand()*100)/RAND_MAX;
		bod[i].p.z=(rand()*100)/RAND_MAX;
		bod[i].v.x=(rand()*100)/RAND_MAX;
		bod[i].v.y=(rand()*100)/RAND_MAX;
		bod[i].v.z=(rand()*100)/RAND_MAX;
		bod[i].m=1;
	}

	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cthen);
   clock_gettime(CLOCK_REALTIME, &rthen);
	verlet(bod,M,1,0.1);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cnow);
   clock_gettime(CLOCK_REALTIME, &rnow);
	printf("%.10f %.10f\n",(cnow.tv_nsec-cthen.tv_nsec)*1e-9+cnow.tv_sec-cthen.tv_sec,
								(rnow.tv_nsec-rthen.tv_nsec)*1e-9+rnow.tv_sec-rthen.tv_sec);


	return 0;
}
