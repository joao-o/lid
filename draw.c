#include <stdio.h>

#include <SDL.h>
#include <epoxy/gl.h>
#include <epoxy/glx.h>
#include <GL/glu.h>
#include <SDL_opengl.h>
#include <CL/cl.h>

#include "opencl.h"
#include "onevar.h"
#include "verlet.h"

#include "util.h"

void
dplanet(planet *planet,drawd *dr)
{
	const GLfloat fourones[] = { 1.0, 1.0, 1.0, 1.0 };
	const GLfloat mat_specular[] = { .10, .10, .10, 1.0 };
	const GLfloat basicolr[] = { .10, 0.5, 0.9, 1.0 };
	const GLfloat mat_shininess[] = { 10 };
	const GLfloat reset[] = { 0, 0, 0, 1 };

	static GLfloat colbuf[] = { 0, 0, 0, 1 };

	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, basicolr);

	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

	glPushMatrix();
	glTranslatef(planet->p.x, planet->p.y, planet->p.z);

	if (planet->rad)
		gluSphere(dr->quad, planet->rad * dr->radsc, 15, 15);
	else
		gluSphere(dr->quad, 0.5 * dr->radsc, 15, 15);

	glMaterialfv(GL_FRONT, GL_EMISSION, reset);
	glPopMatrix();

}


void
dghost(one * ring, int s)
{
	/*
	 *
	 glBegin(GL_LINE_STRIP);
	 if(ring->foc == 0 || ring->foc == -1)
	 {
	 if(ring->agc>ring->nghosts)
	 for (int k = ring->gc + 1; k < ring->nghosts; k++)
	 glVertex3f(ring->ghost[s][k * 3]    ,
	 ring->ghost[s][k * 3 + 1],
	 ring->ghost[s][k * 3 + 2]);

	 for (int k = 0; k < ring->gc; k++)
	 glVertex3f(ring->ghost[s][k * 3]    ,
	 ring->ghost[s][k * 3 + 1],
	 ring->ghost[s][k * 3 + 2]);
	 }
	 else
	 {
	 //printf(":(\n");
	 if(ring->agc>ring->nghosts)
	 for (int k = ring->gc + 1; k < ring->nghosts; k++)
	 glVertex3f(ring->ghost[s][k * 3]
	 -ring->ghost[ring->foc-1][k * 3]
	 +ring->x[6*ring->foc-6],
	 ring->ghost[s][k * 3 + 1]
	 -ring->ghost[ring->foc-1][k * 3 + 1]
	 +ring->x[6*ring->foc-6 + 1],
	 ring->ghost[s][k * 3 + 2]
	 -ring->ghost[ring->foc-1][k * 3 + 2]
	 +ring->x[6*ring->foc-6 + 2]);

	 for (int k = 0; k < ring->gc; k++)
	 glVertex3f(ring->ghost[s][k * 3]
	 -ring->ghost[ring->foc-1][k * 3]
	 +ring->x[6*ring->foc-6],
	 ring->ghost[s][k * 3 + 1]
	 -ring->ghost[ring->foc-1][k * 3 + 1]
	 +ring->x[6*ring->foc-6 + 1],
	 ring->ghost[s][k * 3 + 2]
	 -ring->ghost[ring->foc-1][k * 3 + 2]
	 +ring->x[6*ring->foc-6 + 2]);
	 }
	 glEnd();
	 */
}

void
drawbod(planet *bod,drawd *dr,int N)
{
	const GLfloat fourones[] = { 1.0, 1.0, 1.0, 1.0 };
	const GLfloat sun_position[] = { 1.0, 1.0, 1.0, .0 };
	const GLfloat reset[] = { 0, 0, 0, 1 };

	static GLfloat colbuf[] = { 0, 0, 0, 1 };
	static GLfloat posbuf[] = { 0, 0, 0, 1 };
	int light = 0;

	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, reset);

	glLightfv(GL_LIGHT0, GL_DIFFUSE, fourones);
	glLightfv(GL_LIGHT0, GL_SPECULAR, fourones);
	glLightfv(GL_LIGHT0, GL_POSITION, sun_position);
	if (dr->balls)
		for (int i = 0; i < N; i++)
			dplanet(&bod[i],dr);
	else {
		glPointSize(6.0);
		glBegin(GL_POINTS);
		glColor3f(1., 1., 1.);
		glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, fourones);
		for (int i = 0; i < N; i++) {
			glVertex3f(bod[i].p.x, bod[i].p.y, bod[i].p.z);
		}
		glEnd();
	}
}

void
focuspl(planet *bod,cam *cam,int N)
/*focus on a planet/center of mass */
{
	switch(cam->f.i) {
		case FZERO:
		cam->f.p.x = 0;
		cam->f.p.y = 0;
		cam->f.p.z = 0;
		break;
		case FCOM:{
		float x = 0, y = 0, z = 0, M = 0;
		for (int i = 0; i < N; i++) {
			x += bod[i].p.x * bod[i].m;
			y += bod[i].p.y * bod[i].m;
			z += bod[i].p.z * bod[i].m;
			M += bod[i].m;
		}
		cam->f.p.x = x / M;
		cam->f.p.y = y / M;
		cam->f.p.z = z / M;
		}
		break;
		default:
		if (cam->f.i < N) {
			cam->f.p.x = bod[cam->f.i].p.x;
			cam->f.p.y = bod[cam->f.i].p.y;
			cam->f.p.z = bod[cam->f.i].p.z;
		}
		break;
		}

}

void
focswitch(focus *foc, int N,int dn)
{
	foc->i += dn;
	if (foc->i < -2 ) foc->i = -2;
	if (foc->i > N ) foc->i = N;
}

void
setcam(cam* cam)
{
	gluLookAt(cam->ang.r * (sin(cam->ang.t) * cos(cam->ang.p)) + cam->f.p.x, 
				 cam->ang.r * (cos(cam->ang.t)) + cam->f.p.y,
				 cam->ang.r * (sin(cam->ang.t) * sin(cam->ang.p))
				 + cam->f.p.z,
				 cam->f.p.x, cam->f.p.y, cam->f.p.z, 0, 1, 0);
}
