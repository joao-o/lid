#include <stdio.h>
#include <stdlib.h>
#include <SDL.h>

void
sdldie(const char *msg)
{
	printf("%s: %s\n", msg, SDL_GetError());
	SDL_Quit();
	exit(EXIT_FAILURE);
}

void
die(const char *msg)
{
	puts(msg);
	exit(EXIT_FAILURE);
}

void
*xmalloc(size_t size)
{
	void *p;
	p = malloc(size);
	if (p == NULL)
		die("Failed to allocate memory");
	return p;
}


void
*xcalloc(size_t nmemb,size_t size)
{
	void *p;
	p = calloc(nmemb,size);
	if (p == NULL)
		die("Failed to allocate memory");
	return p;
}

void
*xrealloc(void *ptr,size_t size)
{
	void *p;
	p = realloc(ptr,size);
	if (p == NULL)
		die("Failed to allocate memory");
	return p;
}
