#include <CL/cl.h>
#include <epoxy/gl.h>
#include <epoxy/glx.h>
#include <GL/glu.h>
#include <SDL.h>
#include <ft2build.h>
#include FT_FREETYPE_H

#include "opencl.h"
#include "util.h"
#include "onevar.h"
#include "periapsis.h"
#include "stdarg.h"

static FT_Face face;
static FT_Library ft;

GLuint tex;


void init_font(char *path)
{
	if (FT_Init_FreeType(&ft))
		die("font init failed");
	if(FT_New_Face(ft,path, 0, &face))
		die("failed to open font");
	return;
}

float	
draw_string(const char *s,GLfloat x,GLfloat y, GLuint size, const GLfloat color[3])
{
	float w,h,x2,y2;
	unsigned int i;
	FT_GlyphSlot g;
	float x0=x;

	FT_Set_Pixel_Sizes(face, 0, size);
	glColor3fv(color);

	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, tex);

	for(i=0;s[i]!=0;i++) {
		if(s[i]=='\n') {
			x=x0;
			y-=size*P2CY*1.5;
			continue;
		}

		FT_Load_Char(face, s[i], FT_LOAD_RENDER);
		g = face->glyph;
		x2= x + g->bitmap_left*P2CX;
		y2= y + g->bitmap_top*P2CY;
		w=(float)(g->bitmap.width)*P2CX;
		h=(float)(g->bitmap.rows)*P2CY;

		glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, g->bitmap.width, g->bitmap.rows,
	                0, GL_ALPHA, GL_UNSIGNED_BYTE, g->bitmap.buffer);
	
		glBegin(GL_QUADS);
		glTexCoord2f(0.0, 0.0); glVertex2f(x2,  y2);
		glTexCoord2f(1.0, 0.0); glVertex2f(x2+w,y2);
		glTexCoord2f(1.0, 1.0); glVertex2f(x2+w,y2-h);
		glTexCoord2f(0.0, 1.0); glVertex2f(x2,  y2-h);
		glEnd();
		x+=(float)(g->advance.x)/64.0*P2CX;
		y+=(float)(g->advance.y)/64.0*P2CY;
	}
	glFlush();
	glDisable(GL_TEXTURE_2D);
	return x;
}

#define B_SZ 200
float scrprintf(GLfloat x,GLfloat y,GLuint size, 
					const GLfloat color[3],const char *fmt,...)
{
	char buf[B_SZ];
	va_list argptr;
	va_start(argptr,fmt);
	vsnprintf(buf,B_SZ,fmt,argptr);
	va_end(argptr);
	return draw_string(buf,x,y,size,color);
}
