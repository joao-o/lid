typedef struct _list list;

enum {
	ROOT,   /* ROOT is for the root node only */
	PARENT, /* node has a pointer to the parent menu */
	LIST,   /* node has a pointer to a sub menu */
	FLAG,   /* pointer to a binary flag */
	FLOAT,  /* pointer to a float */
	INT,
	INIT,
	STRING  /* pointer to char (null terminated string) */
};
/* data types */

enum {
	S_DYN,
	S_FIX
};
/* type of string 
 * dynamic allocated vs staticly allocated */

struct _list {
	uint8_t st :1; /* type of string */
	uint8_t dt :7; /* data type on node */
	union { /* data on the node */
		float *f;
		int *i;
		list *l;
		uint8_t *flg;
		char *s;
	} d;
	list *prev; /* ptr to next node */
	void (*f)(void*); /* callback function */
	list *next; /* ptr to previous node */
	size_t sl; /* size of name string */
	char *s; /* name string that will be displayed */
};

list *linsert(list *lst, uint8_t type, void *data,void (*call)(void*),
		        const char *str,size_t n);
list *lcreate(list *next,list *prev,uint8_t type, 
		        void *data,void (*call)(void*),const char *str,size_t n);
void l_rm(list *l);
char *get_lstr(list *l);
int l_sz(list *l);
