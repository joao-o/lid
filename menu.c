#include <SDL.h>
#include <epoxy/gl.h>
#include <epoxy/glx.h>
#include <GL/glu.h>
#include <SDL_opengl.h>
#include <CL/cl.h>
#include "opencl.h"

#include "list.h"
#include "font.h"
#include "onevar.h"
#include "periapsis.h"
#include "menu.h"

#define TITLE_H 50
#define ENTRY_H 30

#define B_SZ 80
#define STR(x) x,sizeof(x)

extern int a;

/* static global variables */
static list *c_entry;
static list *c_menu;

/* c_entry holds the currently highlighted list */
/* c_menu holds the menu where we currently are */

const GLfloat white[] = {1,1,1};
const GLfloat green[] = {0.9,0.9,0};

int num[] = {1,1,1,1,1,1,1,1,1,1};

list 
*insert_menu(list *lst,const char *str, size_t n)
{
	list *menu,*pmenu;
	menu=linsert(lst,LIST,NULL,NULL,str,n);
	pmenu=lcreate(NULL,NULL,PARENT,menu,NULL,STR("<- Back"));
	menu->d.l=pmenu;
	pmenu->next=pmenu;
	pmenu->prev=pmenu;
	return pmenu;
}

void 
init_menu(one *ring)
{
	list *root,*nup,*npcz,*npv,*nps;

	nup=lcreate(NULL,NULL,PARENT,NULL,NULL,
		    STR("Press ESC to leave menu"));
	root=lcreate(NULL,NULL,ROOT,nup,NULL,
		     STR("PERIAPSIS - Menu"));
	nup->d.l=root;
	nup->next=nup;
	nup->prev=nup;

	npcz=insert_menu(nup,STR("Camera Position & Zoom"));
	npv=insert_menu(nup,STR("Visualisation"));
	nps=insert_menu(nup,STR("Simulation Options"));

	linsert(npcz,FLOAT,&(ring->cam.z)    ,NULL,STR("Zoom (Z/X)"));
	linsert(npcz,FLOAT,&(ring->cam.ang.r),NULL,STR("r (O/I)"));
	linsert(npcz,FLOAT,&(ring->cam.ang.t),NULL,STR("theta (UP/DOWN)"));
	linsert(npcz,FLOAT,&(ring->cam.ang.p),NULL,STR("phi (LEFT/RIGHT)"));

	linsert(npv,FLOAT,&(ring->dr.radsc) ,NULL,STR("Radii size (Y/U)"));
	linsert(npv,FLAG, &(ring->dr.ghf)   ,NULL,STR("Ghost lines (G)"));
	linsert(npv,FLAG, &(ring->dr.balls) ,NULL,STR("Draw spheres (P)"));
	linsert(npv,INT,  &(ring->cam.f.i)  ,NULL,STR("Target (+/-)"));
	linsert(npv,FLAG, &(ring->cam.f.flg),NULL,STR("Hold focus (F)"));

	linsert(nps,FLAG, &(ring->pause) ,NULL,STR("Run (S)"));
	linsert(nps,INT,  &(ring->nstep) ,NULL,STR("Step speed (,/.)"));
	linsert(nps,FLOAT,&(ring->h)     ,NULL,STR("Time step"));

	c_entry=nup;
	c_menu=root;
}

void 
draw_menu()
/*deseja ver menu?*/
{
	float y;
	list *entry;
	char buf[B_SZ];

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	y=-0.1-TITLE_H*P2CY;
	draw_string(c_menu->s,0.2,y,TITLE_H,white);
	y-=TITLE_H*P2CY*1.5;

	entry=c_menu->d.l;
	do {
		if (entry==c_entry){
			draw_string(">",0.2,y,ENTRY_H,green);
			draw_string(entry->s,1,y,ENTRY_H,green);
		} else {
			draw_string(entry->s,1,y,ENTRY_H,white);
		}
		switch(entry->dt){
			case FLOAT:
				snprintf(buf,B_SZ,"%.4g",*(entry->d.f));
				draw_string(buf,5,y,ENTRY_H,white);
				break;
			case INT:
				snprintf(buf,B_SZ,"%d",*(entry->d.i));
				draw_string(buf,5,y,ENTRY_H,white);
				break;
			case FLAG:
				if(*(entry->d.flg))
					draw_string("Yes",5,y,ENTRY_H,white);
				else 
					draw_string("No",5,y,ENTRY_H,white);
				break;
		}
		entry=entry->next;
		y-=ENTRY_H*P2CY*1.2;
	} while(entry->dt!=PARENT);

	glFlush();
}

int 
enter_entry(cam *cam)
{
	list *entry;
	switch(c_entry->dt){
		case LIST:
		case PARENT:
			if (c_entry->d.l->dt!=ROOT) {
				c_entry=c_entry->d.l;
				entry=c_entry;
				while(entry->dt!=PARENT)
					entry=entry->next;
				c_menu=entry->d.l;
			}
			break;
		case FLAG:
			FLIP(*(c_entry->d.flg));
			break;
		case INT:  /* fallthrough */
		case FLOAT:
			edit_loop(cam);
			break;
		case INIT:
			(*c_entry->f)((void *)(c_entry->d.i));
			return 1;
			break;
		default:
			break;
	}
	return 0;
}

void 
draw_edit_menu(const char *buf, unsigned int bpos)
{
	(void) bpos;
	float x,y;
	char oldbuf[B_SZ];

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	y=-0.1-TITLE_H*P2CY;
	draw_string(c_entry->s,0.2,y,TITLE_H,white);
	y-=TITLE_H*P2CY*1.5;

	x=draw_string("old value:",0.2,y,ENTRY_H,white);
	switch(c_entry->dt) {
		case FLOAT:
		snprintf(oldbuf,B_SZ,"%.4g",*(c_entry->d.f));
		break;
		case INT:
		snprintf(oldbuf,B_SZ,"%i",*(c_entry->d.i));
		break;
		default:
		break;
	}

	draw_string(oldbuf,0.2+x,y,ENTRY_H,white);
	y-=ENTRY_H*P2CY*1.5;

	x=draw_string("new value:",0.2,y,ENTRY_H,white);
	draw_string(buf,0.2+x,y,ENTRY_H,white);
	glFlush();
}

void 
edit_loop(cam *cam)
{
	SDL_Event ev;
	char buf[B_SZ+1];
	buf[B_SZ]=0;
	unsigned int bpos=0;

	for(int i=0;i<B_SZ;i++)
		buf[i]=0;

	for(;;){
		while (SDL_PollEvent(&ev)) {
			switch (ev.type) {
			case SDL_KEYDOWN:
				switch (ev.key.keysym.sym) {
				case SDLK_ESCAPE:
					return;
					break;
				case SDLK_RETURN:
					switch (c_entry->dt) {
						case FLOAT:
							*(c_entry->d.f)=strtof(buf,NULL);
							break;
						case INT:
							*(c_entry->d.i)=atoi(buf);
							break;
						default: break;
					}
					return;
					break;
				default:
					if (ev.key.keysym.sym < 255)
						buf[bpos++]=(char)(ev.key.keysym.sym % 0xff);
					break;
				}
				break;
			case SDL_WINDOWEVENT:
				switch (ev.window.event) {
				case SDL_WINDOWEVENT_SIZE_CHANGED:
					cam->w=ev.window.data1;
					cam->h=ev.window.data2;
					resize_window_text(cam);
					break;
				}
			}
		}
		if(bpos > B_SZ)
			bpos=0;
		draw_edit_menu(buf,bpos);
		SDL_GL_SwapWindow(cam->win);
	}
}

void
resize_window_text(cam *cam)
{
		glViewport(0, 0, (GLsizei) cam->w, (GLsizei) cam->h);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, 10.0 / SCR_W * cam->w,
				  -10.0 / SCR_H * cam->h, 0, -5, 5);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		return;
}

int
menu_loop(cam *cam)
{
	int ret=0;
	SDL_Event ev;
	resize_window_text(cam);
	glDisable(GL_LIGHTING);

	for(;;){
		while (SDL_PollEvent(&ev)) {
			switch (ev.type) {
			case SDL_KEYDOWN:
				switch (ev.key.keysym.sym) {
				case SDLK_ESCAPE:
					ret=1;
					goto _exit;
					break;
				case SDLK_DOWN:
					c_entry=c_entry->next;
					break;
				case SDLK_UP:
					c_entry=c_entry->prev;
					break;
				case SDLK_RETURN:
					if(enter_entry(cam))
						goto _exit;
					break;
				}
				break;
			case SDL_WINDOWEVENT:
				switch (ev.window.event) {
				case SDL_WINDOWEVENT_SIZE_CHANGED:
					cam->w=ev.window.data1;
					cam->h=ev.window.data2;
					resize_window_text(cam);
					break;
				}
			}
		}
	draw_menu();
	SDL_GL_SwapWindow(cam->win);
	}
_exit:
	resize_window(cam);
	glEnable(GL_LIGHTING);
	return ret;
}
