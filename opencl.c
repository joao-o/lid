#define _POSIX_C_SOURCE 200809L
#include <CL/cl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "opencl.h"
#include "util.h"

void
ckoclerr(const char *msg,cl_int error)
{
	if (error != CL_SUCCESS) {
		printf("failed at %s with error %d\n",msg, error);
		exit(-1);
	}
}

void
p_platinfos(cl_platform_id id, cl_uint i)
{
	size_t size;
	char *s;
	clGetPlatformInfo(id, CL_PLATFORM_NAME, 0, NULL, &size);
	s = xmalloc(sizeof(char) * size);
	clGetPlatformInfo(id, CL_PLATFORM_NAME, size, s, NULL);
	printf("%d platform: %s \n", i + 1, s);
	free(s);
	return;
}

void
p_devinfos(cl_device_id id, cl_uint i, cl_device_info param_name, char *msg)
{
	size_t size;
	char *s;
	clGetDeviceInfo(id, param_name, 0, NULL, &size);
	s = xmalloc(sizeof(char) * size);
	clGetDeviceInfo(id, param_name, size, s, NULL);
	printf("%d%s%s\n", i + 1, msg, s);
	free(s);
	return;
}

cl_program
read_program(const char *path, cl_context context)
{
	FILE *f = fopen(path, "r");
	char buf[200];
	int count = 0;
	int i, j;
	size_t *sizes;
	char **strings;
	cl_int err;
	cl_program program;

	for (count = 0; fgets(buf, 200, f) != NULL; count++);

	sizes = xmalloc(sizeof(size_t) * count);
	strings = xmalloc(sizeof(char *) * count);

	rewind(f);
	for (j = 0; j < count; j++) {
		fgets(buf, 200, f);
		for (i = 0; buf[i] != '\n'; i++);
		sizes[j] = i;
		strings[j] = strndup(buf, i);
	}

	program = clCreateProgramWithSource(context, count, (const char **)strings,
													sizes, &err);
	ckoclerr("Create Program",err);

	for (j = 0; j < count; j++)
		free(strings[j]);
	free(strings);
	free(sizes);
	return program;
}

void
build_kernel(ocl_dat *ocl,char *file)
{
	cl_int err = CL_SUCCESS;
	cl_build_status b_stat;
	size_t b_logsize;
	char *b_log;

	ocl->program = read_program(file, ocl->context);
	err = clBuildProgram (ocl->program, ocl->n_devices, 
											  ocl->devices, "-cl-nv-verbose", NULL, NULL);
	err = clGetProgramBuildInfo(ocl->program,ocl->devices[0],CL_PROGRAM_BUILD_STATUS,
								 sizeof(b_stat),&b_stat,NULL);
	ckoclerr("build info query 1",err);

	err = clGetProgramBuildInfo(ocl->program,ocl->devices[0],CL_PROGRAM_BUILD_LOG,
								 0,NULL,&b_logsize);
	ckoclerr("build info query 2",err);

	b_log=xmalloc(sizeof(char)*b_logsize);
	err = clGetProgramBuildInfo(ocl->program,ocl->devices[0],CL_PROGRAM_BUILD_LOG,
								 b_logsize,b_log,&b_logsize);
	ckoclerr("build info query 3",err);
	puts(b_log);
	free(b_log);

	if(b_stat != CL_BUILD_SUCCESS)
		die("build failed");

	puts("openCL kernel compiled");
	return;
}


void
init_openCL(ocl_dat *ocl)
{
	cl_int err = CL_SUCCESS;
	cl_uint i;

	/* check for platforms */
	clGetPlatformIDs(0, NULL, &(ocl->n_platforms));

	if (ocl->n_platforms == 0)
		die("error: no platforms detected\n");
	ocl->platforms = xmalloc(sizeof(ocl->platforms) * ocl->n_platforms);
	clGetPlatformIDs(ocl->n_platforms, ocl->platforms, NULL);

	for (i = 0; i < ocl->n_platforms; i++)
		p_platinfos(ocl->platforms[i], i);

	/* check for devices */
	clGetDeviceIDs(ocl->platforms[0], CL_DEVICE_TYPE_ALL, 
						0, NULL, &(ocl->n_devices));
	ocl->devices = xmalloc(sizeof(ocl->devices) * ocl->n_devices);
	clGetDeviceIDs(ocl->platforms[0], CL_DEVICE_TYPE_ALL, ocl->n_devices,
						ocl->devices, NULL);

	for (i = 0; i < ocl->n_devices; i++){
		p_devinfos(ocl->devices[i], i,CL_DEVICE_NAME,"-Device name:");
		p_devinfos(ocl->devices[i], i,CL_DEVICE_VERSION,"-Device ver:");
		p_devinfos(ocl->devices[i], i,CL_DRIVER_VERSION,"-Driver ver:");
	}

	/* make context using first found device ...*/
	ocl->context_properties[0] = CL_CONTEXT_PLATFORM;
	ocl->context_properties[1] = (cl_context_properties) (ocl->platforms[0]);
	ocl->context_properties[2] = 0;
	ocl->context_properties[3] = 0;

	ocl->context = clCreateContext(ocl->context_properties, ocl->n_devices,
											 ocl->devices, NULL, NULL, &err);
	ckoclerr("create context",err);

	build_kernel(ocl,"kernel.cl");
	
	ocl->kernel = clCreateKernel(ocl->program, "SAXPY", &err);
	ckoclerr("create kernel",err);

	ocl->queue = clCreateCommandQueue(ocl->context, ocl->devices[0],0, &err);
	ckoclerr("create command queue",err);

	puts("openCL initialization complete ");

	return;
}

void
release_opencl(ocl_dat *ocl)
{
	clReleaseCommandQueue(ocl->queue);
	clReleaseKernel(ocl->kernel);
	clReleaseProgram(ocl->program);
	clReleaseContext(ocl->context);
	return;
}
