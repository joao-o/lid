#define  _POSIX_C_SOURCE 200809L
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"
#include "list.h"
	
list
*linsert(list *lst, uint8_t type,void *data,void (*call)(void*),
		   const char *str,size_t n)
{
	list *l;
	l=lcreate(lst,lst->prev,type,data,call,str,n);
	lst->prev->next=l;
	lst->prev=l;
	return l;
}

list
*lcreate(list *next,list *prev,uint8_t type,void *data,void (*call)(void*),
		   const char *str,size_t n)
{
	list *l;
   l = xmalloc(sizeof(list));
	l->s = strndup(str,sizeof(char)*n);
	l->sl = n;
	l->st = S_DYN;
   l->dt = type;
	l->f=call;
	switch(type) {
		case ROOT:
		case PARENT:  /* fallthrough */
		case LIST:
			l->d.l=(list *)data;
			break;
		case FLAG:
			l->d.flg=(uint8_t *)data;
			break;
		case FLOAT:
			l->d.f=(float*)data;
			break;
		case INIT:
		case INT:
			l->d.i=(int*)data;
			break;
		case STRING:
			l->d.s=(char*)data;
			break;
		default:
			die("unknown type at l_create");
	}
	l->prev = prev;
   l->next = next;
	return l;
}

void l_rm(list *l)
{
   if (!l->next) {
      free(l);
	} else {
		if (l->st==S_FIX)
			free(l->s);
      switch(l->dt){
         case LIST:
				l_rm(l->d.l);
            break;
         case STRING:
            free(l->d.s);
            break;
			default:
				break;
		}
		l_rm(l->next);
   } 
}


int l_sz(list *l)
{
   int i;
   for (i = 0; l != NULL; i++)
      l = l->next;
   return i;
}

