/* Screen dimension constants */
#define SCR_W  640
#define SCR_H  640

/* pixel to screen coordinates*/
#define P2CX (10.0f/SCR_W) 
#define P2CY (10.0f/SCR_H)

#define FLIP(x) (x)^=1

void resize_window(cam *cam);
